﻿create table Shop_ProductMasterGLO (ID int identity(1,1),ProductName Varchar(500),ProductDescription  Varchar(500),Price money,
NumberOfItemsInStock int)

GO

create Proc USP_GetAllProductDetails
AS 
BEGIN

Select ID,ProductName,ProductDescription,Price,NumberOfItemsInStock from Shop_ProductMasterGLO

END

GO

create Proc USP_SaveProductDetails(

@ProductName varchar(500),
@ProductDescription varchar(500),
@Price Money,
@NumberOfItemsInStock int,
@ID int,
@IsEdit int
)
AS 
BEGIN
If(@IsEdit=0)
BEGIN
Insert into Shop_ProductMasterGLO(
ProductName,ProductDescription,Price,NumberOfItemsInStock)

values(@ProductName,@ProductDescription,@Price,@NumberOfItemsInStock)
END
ELSE If(@IsEdit=1)
BEGIN
Update Shop_ProductMasterGLO SET
ProductName=@ProductName
,ProductDescription=@ProductDescription,
Price=@Price,NumberOfItemsInStock=@NumberOfItemsInStock
where ID=@ID
END
ELSE 
BEGIN
Delete from Shop_ProductMasterGLO where ID=@ID
END

END