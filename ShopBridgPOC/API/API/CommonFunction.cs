﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace API
{

    
    public class CommonFunction
    {
        Dapper objdapper = new Dapper();
        
        public async Task<List<ProductDTO>> GetProductDetails()
        {
            try
            {
                using (QueryDetails queryDetails = new QueryDetails())
                {
                    queryDetails.commandType = CommandType.StoredProcedure;
                    queryDetails.CommandText = "USP_GetAllProductDetails";
                    return (List<ProductDTO>) await objdapper.ExecuteProcedure(queryDetails);

                }
            }
            catch (Exception ex )
            {
                throw ex;
            }
}
        public async Task<int> SaveProductData(ProductDTO objProductDTO)
        {
            try
            {
                using (QueryDetails queryDetails = new QueryDetails())
                {

                    queryDetails.commandType = CommandType.StoredProcedure;
                    queryDetails.CommandText = "USP_SaveProductDetails";
                    queryDetails.parameters.Add("ProductName", dbType: DbType.String, value: objProductDTO.ProductName, direction: ParameterDirection.Input);
                    queryDetails.parameters.Add("ProductDescription", dbType: DbType.String, value: objProductDTO.ProductDescription, direction: ParameterDirection.Input);
                    queryDetails.parameters.Add("NumberOfItemsInStock", dbType: DbType.Int32, value: objProductDTO.NumberOfItemsInStock, direction: ParameterDirection.Input);
                    queryDetails.parameters.Add("Price", dbType: DbType.Double, value: objProductDTO.Price, direction: ParameterDirection.Input);
                    queryDetails.parameters.Add("ID", dbType: DbType.Int32, value: objProductDTO.ID, direction: ParameterDirection.Input);
                    queryDetails.parameters.Add("IsEdit", dbType: DbType.Int32, value: objProductDTO.IsEdit, direction: ParameterDirection.Input);

                    return await objdapper.ExecuteNonQueryAsync(queryDetails); ;

                }
            }
            catch (Exception ex )
            {
                throw ex;
            }
        }
    }
}