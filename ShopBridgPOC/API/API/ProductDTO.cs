﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API
{
    public class ProductDTO
    {
        public int ID {get;set;}
        public string ProductName { get; set; }

        public string ProductDescription { get; set; }
        public double Price { get; set; }

        public int NumberOfItemsInStock { get; set; }
        public int IsEdit { get; set; }


    }
}