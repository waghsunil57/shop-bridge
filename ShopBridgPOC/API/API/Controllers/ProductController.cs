﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class ProductController : ApiController
    {

        CommonFunction objCommonFuncion = new CommonFunction();
        public HttpResponseMessage Get(int? id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, objCommonFuncion.GetProductDetails());
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
           [HttpPost]
        public HttpResponseMessage SaveProductData(ProductDTO objProductDTO)
        {
            try
            {
                var a = objCommonFuncion.SaveProductData(objProductDTO);
                return Request.CreateResponse(HttpStatusCode.OK, a);
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        
    }
}
