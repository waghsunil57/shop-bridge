﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Configuration;


namespace API
{
    public class Dapper
    {

        string _connection = string.Empty;


        /// <summary>
        /// This async method is used to get number of affected rows
        /// </summary>
        /// <param name="queryDetails">SP input parameters</param>
        /// <returns>It return the number of affected rows</returns>
        public async Task<int> ExecuteNonQueryAsync(QueryDetails queryDetails)
        {

            _connection = ConfigurationManager.ConnectionStrings["ShopBridgeConnectionString"].ToString();
            using (IDbConnection db = new SqlConnection(_connection))
            {
                var result = await db.ExecuteAsync(queryDetails.CommandText, queryDetails.parameters, null, commandTimeout: 0, commandType: queryDetails.commandType);
                return result;
            }
        }

        /// <summary>
        /// This async method is used to execute multiple queries within the same command and map results asynchronously
        /// </summary>
        /// <param name="queryDetails">SP input parameters</param>
        /// <returns>It returns list of dynamic object</returns>

        public async Task<List<ProductDTO>> ExecuteProcedure(QueryDetails queryDetails)
        {

            _connection = ConfigurationManager.ConnectionStrings["ShopBridgeConnectionString"].ToString();
            using (IDbConnection db = new SqlConnection(_connection))
            {
                try
                {

                    List<ProductDTO> resultList = new List<ProductDTO>();
                    resultList = db.Query<ProductDTO>(queryDetails.CommandText, queryDetails.parameters, commandType: CommandType.StoredProcedure).ToList();

                  
                    return resultList;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    db.Dispose();
                }
            }
        }


    }

    
    public class QueryDetails :  IDisposable
    {
        public new void Dispose() { }
        public QueryDetails()
        {
            parameters = new DynamicParameters();
        }

        public DynamicParameters parameters { get; set; }
        public CommandType commandType { get; set; }
        public string CommandText { get; set; }
    }
}